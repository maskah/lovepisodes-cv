var mongoose = require('mongoose');

var seasonSchema = new mongoose.Schema({
  name: String,
  pathName: String,
  tvshow: {type:mongoose.Schema.Types.ObjectId, ref:'TVShow'},
  episodes: [{type:mongoose.Schema.Types.ObjectId, ref:'Episode'}]
});

/**
 * Set path name
 */
seasonSchema.pre('save', function(next) {
  this.pathName = this.name.replace(/ /g, '-').toLowerCase();
  next();
});

module.exports = mongoose.model('Season', seasonSchema);