var mongoose = require('mongoose');

var episodeSchema = new mongoose.Schema({
  name: String,
  pathName: String,
  outDate: Date,
  spoilers: String,
  urlPreview: String,
  season: {type:mongoose.Schema.Types.ObjectId, ref:'Season'},
  tvshow: {type:mongoose.Schema.Types.ObjectId, ref:'TVShow'},
  votes: [Number],
  numberOfComments: {type:Number, default:0}
});

/**
 * Set path name
 */
episodeSchema.pre('save', function(next) {
  this.pathName = this.name.replace(/ /g, '-').toLowerCase();
  next();
});

module.exports = mongoose.model('Episode', episodeSchema);