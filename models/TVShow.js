var mongoose = require('mongoose');

var tvshowSchema = new mongoose.Schema({
  name: String,
  pathName: String,
  mainImgPath: String,
  coverImgPath: String,
  starTVShow: { type: Boolean, default: 'false' },
  seasons: [{type:mongoose.Schema.Types.ObjectId, ref:'Season'}],
  numberOfComments: {type:Number, default:0}
});

/**
 * Set path name
 */
tvshowSchema.pre('save', function(next) {
  this.pathName = this.name.replace(/ /g, '-').toLowerCase();
  next();
});

module.exports = mongoose.model('TVShow', tvshowSchema);
