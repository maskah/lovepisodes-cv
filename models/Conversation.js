var mongoose = require('mongoose');

var conversationSchema = new mongoose.Schema({
  users: [{type:mongoose.Schema.Types.ObjectId, ref:'User'}],
  lastMessage: {type:mongoose.Schema.Types.ObjectId, ref:'Message'},
  lastMessageDate: Date
});

module.exports = mongoose.model('Conversation', conversationSchema);