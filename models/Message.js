var mongoose = require('mongoose');

var messageSchema = new mongoose.Schema({
  content: String,
  author: {type:mongoose.Schema.Types.ObjectId, ref:'User'},
  conversation: {type:mongoose.Schema.Types.ObjectId, ref:'Conversation'},
  date: { type: Date, default:Date.now}
});

module.exports = mongoose.model('Message', messageSchema);