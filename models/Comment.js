var mongoose = require('mongoose');

var commentSchema = new mongoose.Schema({
  content: String,
  author: {type:mongoose.Schema.Types.ObjectId, ref:'User'},
  date: { type: Date, default:Date.now},
  episode: {type:mongoose.Schema.Types.ObjectId, ref:'Episode'},
  season: {type:mongoose.Schema.Types.ObjectId, ref:'Season'},
  tvshow: {type:mongoose.Schema.Types.ObjectId, ref:'TVShow'}
});

module.exports = mongoose.model('Comment', commentSchema);