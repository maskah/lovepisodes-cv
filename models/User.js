var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  email: { type: String, unique: true, lowercase: true },

  facebook: String,
  twitter: String,
  google: String,
  github: String,
  instagram: String,
  linkedin: String,
  tokens: Array,

  profile: {
    name: { type: String, default: '' },
    picture: { type: String, default: '' },
    thumbnail: { type: String, default: '' }
  },

  stats: {
    interventions: [{
      id: {type:mongoose.Schema.Types.ObjectId, ref:'TVShow'},
      number: {type: Number, default:0}
    }],
    numberOfComments: {type: Number, default: 0},
    votedEpisodes: [{type:mongoose.Schema.Types.ObjectId, ref:'Episode'}]
  },

  superadmin: {type: Boolean, default: false},

  messageNotifications : {type: Number, default: 0},
  nonReadConversations : [{type:mongoose.Schema.Types.ObjectId, ref:'Conversation'}],

  inscriptionDate: {type: Date, default:Date.now},

  resetPasswordToken: String,
  resetPasswordExpires: Date
});

/**
 * Password hash middleware.
 */
userSchema.pre('save', function(next) {
  var user = this;
  if (!user.isModified('password')) return next();
  bcrypt.genSalt(10, function(err, salt) {
    if (err) return next(err);
    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});

userSchema.pre('save', function(next) {
  if(this.profile.name == '')
    this.profile.name = this.email;
  next();
});

module.exports = mongoose.model('User', userSchema);
