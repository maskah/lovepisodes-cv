var colors = ['#ff8409', '#ffff5e', '#57cbf3', '#25ee5a', '#ff4a99'];

function createPieChart(data){
	var newopts = {
		inGraphDataShow : true, 
	    inGraphDataAnglePosition : 2,
	    inGraphDataRadiusPosition: 2,
	    inGraphDataRotate : "inRadiusAxisRotateLabels",
	    inGraphDataAlign : "center",
	    inGraphDataVAlign : "middle",
	    inGraphDataFontColor : '#555555',
	    inGraphDataFontSize : 14,
	    inGraphDataTmpl : "<%=v1%>"
	}

	var pieCtx = document.getElementById('chart').getContext('2d');
	new Chart(pieCtx).Pie(data, newopts);
}

     
function infiniteScroll(currentPage){
  var page = 2;
   
  // on initialise ajaxready à true au premier chargement de la fonction
  $(window).data('ajaxready', true);
  $(window).scroll(function() {
	  // On teste si ajaxready vaut false, auquel cas on stoppe la fonction
	  if ($(window).data('ajaxready') == false) return;
	  if(($(window).scrollTop() + $(window).height()) + 150 > $(document).height()) {
		// lorsqu'on commence un traitement, on met ajaxready à false
		$(window).data('ajaxready', false);

		//detecter s'il y a déjà des queries
		var separator = '?';
		if(window.location.search != '')
			separator = '&';

		$.get(window.location.pathname+window.location.search+separator+'page='+page, function(data){
			if (data != '') {
			  page+= 1;
			  // une fois tous les traitements effectués,
			  // on remet ajaxready à false
			  // afin de pouvoir rappeler la fonction
			  $(window).data('ajaxready', true);
			}
			console.log(data);

			var removeButtonHTML = '';
			for(i in data){
				if(currentPage == 'episode' && data[i].superadmin){
					removeButtonHTML = '<div class="col-sm-12"><a href="episode-9/delete-comment/'+data[i]._id+'"><button class="btn btn-danger">Supp. commentaire</button></a></div>'
				}
				if(currentPage == 'episode')
					$('#comments-container').append('<div itemprop="comment" itemscope="" itemtype="http://www.schema.org/Comment" class="row comment"><div class="col-xs-2"><a href="/utilisateurs/'+data[i].author._id+'"><span itemprop="author" itemscope="" itemtype="http://www.schema.org/Person"><img src="'+data[i].author.profile.thumbnail+'" alt="avatar utilisateur" class="thumbnail-comment"><div itemprop="name" class="comment-author">'+data[i].author.profile.name+'</div></span></a></div><div class="col-xs-offset-1 col-xs-9">'+removeButtonHTML+'<div class="comment-date">'+data[i].date+',</div><meta itemprop="datePublished" content="'+data[i].ISODate+'"><meta itemprop="dateCreated" content="'+data[i].ISODate+'"><span itemprop="text"><p>'+data[i].content+'</p></span></div></div>');
				else if(currentPage == 'conversation')
					$('#comments-container').append('<div class="row comment"><div class="col-xs-2"><a href="/utilisateurs/'+data[i].author._id+'"><img src="'+data[i].author.profile.thumbnail+'" alt="avatar utilisateur" class="thumbnail-comment"><div class="author-name">'+data[i].author.profile.name+'</div></a></div><div class="col-xs-offset-1 col-xs-9"><div class="comment-date">'+data[i].date+',</div><p>'+data[i].content+'</p></div></div>');
				else if(currentPage == 'home')
					$('#episodes-container').append('<div class="col-sm-4 home-episode"><div style="background-image:url('+data[i].tvshow.mainImgPath+');" class="home-episode-image"><div class="home-episode-content"><a href="'+'/series/'+data[i].tvshow.pathName+'/'+data[i].season.pathName+'/'+data[i].pathName+'"><div class="home-episode-name">'+data[i].season.name+' &gt; '+data[i].name+'</div><div class="home-episode-info">'+data[i].date+' | '+data[i].tvshow.name+' | '+data[i].numberOfComments+' <i class="fa fa-comment"></i></div></a></div></div></div>');
				else if(currentPage == 'tvshows')
					$('#tvshows-container').append('<div class="col-sm-4 home-episode"><div style="background-image:url('+data[i].mainImgPath+');" class="home-episode-image"><div class="home-episode-content"><a href="/series/'+data[i].pathName+'"><div class="home-episode-name">'+data[i].name+'</div><div class="home-episode-info">'+data[i].numberOfComments+'<i class="fa fa-comment"></i></div></a></div></div></div>')
			}
		});
	  }
  });
};

function rateEpisode(){
	$('.starrr').on('starrr:change', function(e, value){
		window.location = window.location + '/noter?note=' + value;
	});
};

function urlify(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return '<a href="' + url + '">' + url + '</a>';
    })
};

/* $(document).ready(function() {

  var socket = io.connect('ws://localhost:3000/');
  socket.on('newcomment', function (data) {
   console.log(data);
  });

});

*/