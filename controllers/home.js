var TVShow = require('../models/TVShow');
var Season = require('../models/Season');
var Episode = require('../models/Episode');
var moment = require('moment');
var momentFr = require('moment/locale/fr');

moment.locale('fr', momentFr);

exports.index = function(req, res) {

	if(req.query.page) return exports.getNextEpisode(req, res);

	Episode.find({outDate: {'$lt': exports.addDays(new Date(), 6)}}).sort({outDate: -1}).limit(21).populate('season').populate('tvshow').exec(function(err, episodes) {

		for(var i=0; i<episodes.length; i++){
			var date = episodes[i].outDate;
			episodes[i].date = moment(date).fromNow();
		}

		res.render('home', {
			title: 'Accueil - Les derniers épisodes',
			description: 'La programmation des épisodes de séries TV diffusés cette semaine aux Etats-Unis et en France.',
			episodes: episodes
		});
	});
};

exports.getNextEpisode = function(req, res) {
	Episode.find({outDate: {'$lt': exports.addDays(new Date(), 6)}}).sort({outDate: -1}).skip((req.query.page-1)*21).limit(21).populate('season').populate('tvshow').exec(function(err, episodes) {

		for(var i=0; i<episodes.length; i++){
			var date = episodes[i].outDate;
			episodes[i].date = moment(date).fromNow();
		}

		res.json(episodes);
	});
}

//méthode pour ajouter des jours à la date et permettre d'afficher les épisodes qui sortent dans les x jours qui viennnet
exports.addDays = function(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    console.log(result);
    return result;
};