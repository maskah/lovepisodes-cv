var _ = require('lodash');
var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var passport = require('passport');
var User = require('../models/User');
var secrets = require('../config/secrets');

var TVShow = require('../models/TVShow');
var Season = require('../models/Season');
var Episode = require('../models/Episode');
var Comment = require('../models/Comment');

var moment = require('moment');
var momentFr = require('moment/locale/fr');

moment.locale('fr', momentFr);

/**
 * GET /login
 * Login page.
 */
exports.getLogin = function(req, res) {
  if (req.user) return res.redirect('/');
  res.render('account/login', {
    title: 'Connexion'
  });
};

/**
 * GET /logout
 * Log out.
 */
exports.logout = function(req, res) {
  req.logout();
  res.redirect('/');
};

/**
 * GET /account
 * Profile page.
 */
exports.getAccount = function(req, res) {
  res.render('account/profile', {
    title: 'Gestion du compte'
  });
};

/**
 * POST /account/profile
 * Update profile information.
 */
exports.postUpdateProfile = function(req, res, next) {

  req.assert('email', 'Email invalide').isEmail().len(1,40);
  req.assert('name', 'Le pseudo doit contenir entre un et 20 caractères').len(1,20);

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/account');
  }

  User.findById(req.user.id, function(err, user) {
    if (err) return next(err);
    user.email = req.body.email || '';
    user.profile.name = req.body.name || '';

    user.save(function(err) {
      if (err) return next(err);
      req.flash('success', { msg: 'Informations du profil mises à jour' });
      res.redirect('/account');
    });
  });
};

/**
 * POST /account/delete
 * Delete user account.
 */
exports.postDeleteAccount = function(req, res, next) {
  User.remove({ _id: req.user.id }, function(err) {
    if (err) return next(err);
    req.logout();
    req.flash('info', { msg: 'Compte supprimé' });
    res.redirect('/');
  });
};

/**
 * GET /account/unlink/:provider
 * Unlink OAuth provider.
 */
exports.getOauthUnlink = function(req, res, next) {
  var provider = req.params.provider;
  User.findById(req.user.id, function(err, user) {
    if (err) return next(err);

    user[provider] = undefined;
    user.tokens = _.reject(user.tokens, function(token) { return token.kind === provider; });

    user.save(function(err) {
      if (err) return next(err);
      req.flash('info', { msg: provider + ' retiré' });
      res.redirect('/account');
    });
  });
};

exports.isAdmin = function(req, res, next){
  if(req.user.superadmin){
    next();
  }
  res.status(403);
};

exports.getProfile = function(req, res){
  User.findOne({'_id':req.params.user_id}).populate('stats.interventions.id').exec(function(err, user){

    if (user == null || err) {
      return res.sendStatus(404);
    }

    res.render('profile', {
      title: 'Profil de '+user.profile.name,
      description: 'Statistiques et séries préférées de '+user.profile.name+'. Êtes-vous interessé(e) par '+user.profile.name+' ? Envoyez lui un message privé.',
      _user: user,
      numberOfVotes: user.stats.votedEpisodes.length,
      numberOfComments: user.stats.numberOfComments,
      inscriptionDate: moment(user.inscriptionDate).fromNow()
    });
  });
};

exports.getRandomProfile = function(req, res){
  User.count().exec(function(err, count){
    if(err) return next(err);
    var random = Math.floor(Math.random() * count);
    User.findOne().skip(random).exec(function (err, user) {
      if(err) return next(err);
      return res.redirect('/utilisateurs/'+user._id)
    });
  });
};