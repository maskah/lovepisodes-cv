var fs = require('fs');
var async = require('async');
var TVShow = require('../models/TVShow');
var Season = require('../models/Season');
var Episode = require('../models/Episode');
var Comment = require('../models/Comment');

exports.getTVShows = function(req, res) {

	if(req.query.page) return exports.getNextTVShows(req, res);

	var query = TVShow.find().sort({'numberOfComments' : '-1'});
	var criteria = {
		'name': {
		  	'$regex': req.query.input_tvshow,
		  	'$options': 'i'
		}
	};

	if(req.query.input_tvshow){
		query = TVShow.find(criteria).sort({'numberOfComments' : '-1'});
	}

  query.limit(21).exec(function(err, tvshows) {
  	if(tvshows.length == 1){
  		res.redirect('series/'+tvshows[0].pathName);
  	}

  	res.render('tvshows', {
  		title: 'Séries TV '+tvshows.length+' résultats',
  		description: 'Recherchez une série sur LOVEpisodes pour accéder aux spoilers et aux discussions du dernier épisode de votre série préférée.',
  		tvshows: tvshows,
  		numberOfResults: tvshows.length
		});
	});
};

exports.getNextTVShows = function(req, res) {
	var query = TVShow.find().sort({'numberOfComments' : '-1'});
	var criteria = {
		'name': {
		  	'$regex': req.query.input_tvshow,
		  	'$options': 'i'
		}
	};

	if(req.query.input_tvshow){
		query = TVShow.find(criteria).sort({'numberOfComments' : '-1'});
	}

  query.skip((req.query.page-1)*21).limit(21).exec(function(err, tvshows) {
  	if(tvshows.length == 1){
  		res.redirect('series/'+tvshows[0].pathName);
  	}

  	res.json(tvshows);

  });
};

exports.createTVShow = function(req, res) {

	req.assert('name', 'Nom requis').notEmpty();

	var errors = req.validationErrors();

	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/series/'+req.params.tvshow_path_name);
	}

	var mainImg = req.files.tvshowimg;
	var coverImg = req.files.tvshowcover;

  	var tvshow = new TVShow({
	    name: req.body.name,
	    mainImgPath: '/uploads/'+mainImg.name,
	    coverImgPath: '/uploads/'+coverImg.name
	});

	tvshow.save(function(err) {
		if (err) return next(err);
		res.redirect('/series');
	});

};

exports.edit = function(req, res) {
	req.assert('name', 'Nom requis').notEmpty();

	var errors = req.validationErrors();

	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/series/'+req.params.tvshow_path_name);
	}

	var tvshow = res.locals.tvshow;

	var mainImg = req.files.tvshowimg;
	var coverImg = req.files.tvshowcover;


	if(req.body.name)
    	tvshow.name = req.body.name;
	if(mainImg){
		require('fs').unlinkSync('public'+tvshow.mainImgPath);
    	tvshow.mainImgPath = '/uploads/'+mainImg.name;
    }
	if(coverImg){
		require('fs').unlinkSync('public'+tvshow.coverImgPath);
    	tvshow. coverImgPath = '/uploads/'+coverImg.name;
    }

	tvshow.save(function(err) {
		if (err) return next(err);
		res.redirect('/series/'+req.params.tvshow_path_name);
	});
};

exports.getTVShow = function(req, res, next) {

	var tvshow = res.locals.tvshow;

	Season.find({tvshow: tvshow}).sort({ 'name': '-1' }).populate('episodes', null, null, { sort: { 'name': '-1' } }).exec(function(err, seasons){
		res.render('tvshow', {
	  		title: tvshow.name+' : les saisons',
	  		description: 'Découvrez la liste des saisons de la série '+ tvshow.name,
	  		tvshow: tvshow,
	  		seasons: seasons
		});
	});
};

exports.addSeason = function(req, res) {

	req.assert('num', 'Numéro requis').notEmpty();

	var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/series/'+req.params.tvshow_path_name);
  }

  var tvshow = res.locals.tvshow;

	var season = new Season({
    name: 'Saison ' + req.body.num,
    tvshow: tvshow
	});

  season.save(function(err) {
    if (err) return next(err);
    tvshow.seasons.push(season);
    tvshow.save(function(err) {
    	if(err) return next(err);
    	res.redirect('/series/'+req.params.tvshow_path_name);
	});
  });

};

exports.remove = function(req, res) {

	var tvshow = res.locals.tvshow;

	async.series([
	  function(next){ Comment.remove({'tvshow': tvshow}).exec(next) },
	  function(next){ Episode.remove({'tvshow': tvshow}).exec(next) },
	  function(next){ Season.remove({'tvshow': tvshow}).exec(next) },
	  function(next){ TVShow.remove({'_id': tvshow._id}).exec(next) }
	], function(err, results){
			if(err)
				req.flash('info', { msg: 'Erreur lors de la suppréssion, réessayez. Erreur : '+err });
			else
				req.flash('info', { msg: 'Série supprimée' });
			res.redirect('/');
	});

};

/*
exports.createTVShow = function(req, res) {

	req.assert('name', 'Nom requis').notEmpty();

	var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/series/'+req.params.tvshow_path_name);
  }

	var file = req.files.tvshowimg;
	console.log(file);

	for(var i=0; i<200; i++){
	  var tvshow = new TVShow({
	    name: Math.random(),
	    mainImgPath: '/uploads/'+file.name
		});

		tvshow.save(function(err) {
	    if (err) return next(err);
	    res.redirect('/series');
  	});

  	for(var j=0; j<10; j++){
		  var season = new Season({
		    name: Math.random(),
		    tvshow:tvshow
			});

			season.save(function(err) {
		    if (err) return next(err);
		    res.redirect('/series');
			});

			for(var k=0; k<20; k++){
			  var episode = new Episode({
			    name: Math.random(),
			    tvshow:tvshow,
			    season:season,
			    outDate: new Date()
				});

				episode.save(function(err) {
			    if (err) return next(err);
			    res.redirect('/series');
				});
  		}
  	}
	}

};
*/