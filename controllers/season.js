var async = require('async');
var TVShow = require('../models/TVShow');
var Season = require('../models/Season');
var Episode = require('../models/Episode');
var Comment = require('../models/Comment');

exports.getSeason = function(req, res, next) {

	var tvshow = res.locals.tvshow;
	var season = res.locals.season;

	res.render('season', {
  		title: tvshow.name+' '+season.name+' : les épisodes',
  		description: 'Découvrez la liste des épisodes de la '+season.name+' de la série '+ tvshow.name,
  		tvshow: tvshow,
  		season: season
	});

};

exports.addEpisode = function(req, res) {

	req.assert('num', 'Numéro requis').notEmpty();
	req.assert('year', 'Année requise').notEmpty();
	req.assert('month', 'Mois requis').notEmpty();
	req.assert('day', 'Jour requis').notEmpty();
	req.assert('hour', 'Heure requise').notEmpty();
	req.assert('minute', 'Minutes requise').notEmpty();

	var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/series/'+req.params.tvshow_path_name+'/'+req.params.season_path_name);
  }

	var tvshow = res.locals.tvshow;
	var season = res.locals.season;

	var episode = new Episode({
    name: 'Episode '+req.body.num,
    outDate: new Date(req.body.year, req.body.month-1, req.body.day, req.body.hour, req.body.minute, 0, 0),
    tvshow: tvshow,
    season: season
	});

  episode.save(function(err) {
    if (err) return next(err);
    season.episodes.push(episode);
    season.save(function(err) {
    	if(err) return next(err);
    	res.redirect('/series/'+req.params.tvshow_path_name+'/'+req.params.season_path_name);
	});
  });
};

exports.remove = function(req, res) {

	var season = res.locals.season;

	async.series([
	  function(next){ Comment.remove({'season': season}).exec(next) },
	  function(next){ Episode.remove({'season': season}).exec(next) },
	  function(next){ Season.remove({'_id': season._id}).exec(next) }
	], function(err, results){
			if(err)
				req.flash('info', { msg: 'Erreur lors de la suppréssion, réessayez. Erreur : '+err });
			else
				req.flash('info', { msg: 'Saison supprimée' });
			res.redirect('/');
	});

};