var async = require('async');
var request = require('request');

var secrets = require('../config/secrets');

var User = require('../models/User');
var TVShow = require('../models/TVShow');
var Season = require('../models/Season');
var Episode = require('../models/Episode');
var Comment = require('../models/Comment');

var moment = require('moment');
var momentFr = require('moment/locale/fr');

moment.locale('fr', momentFr);

exports.getEpisode = function(req, res, next) {

	if(req.query.page) return exports.getNextComments(req, res, next);

	var tvshow = res.locals.tvshow;
	var season = res.locals.season;
	var episode = res.locals.episode;
	var numberOfVotes = episode.votes.length;
	var note = Math.round(exports.calculateAverage(episode.votes)*10)/10;
	var roundedNote = Math.round(exports.calculateAverage(episode.votes)) || 0;

	Comment.find({'episode': episode}).sort({'date':req.query.order||-1}).skip((req.query.page-1)*20).limit(20).populate('author').lean().exec(function(err, comments) {

		var date = episode.outDate;
		episode.date = moment(date).fromNow();
		episode.ISODate = date;

		for(i in comments){
			var date = new Date(comments[i].date);
			comments[i].date = moment(date).fromNow();
			comments[i].ISODate = date;
			if(comments[i].author == null){
				comments[i].author = {
					_id: '#',
					profile: {
						name: 'Compte supprimé',
						thumbnail: '/images/deleted_account.jpeg'
					}
				};
			}
		}

		res.render('episode', {
	  		title: tvshow.name+' '+season.name+' '+episode.name+' : '+'Spoilers et Discussions',
	  		description: 'Décrouvez les spoilers et discuter de l\''+episode.name+' de la '+season.name+' de la série'+ tvshow.name,
	  		tvshow: tvshow,
	  		season: season,
	  		episode: episode,
	  		comments: comments,
	  		numberOfVotes: numberOfVotes,
	  		note: note || '-',
	  		roundedNote: roundedNote
		});

	});

};

exports.getNextComments = function(req, res, next) {

	var episode = res.locals.episode;

	Comment.find({'episode': episode}).sort({'date':req.query.order||-1}).skip((req.query.page-1)*20).limit(20).populate('author').lean().exec(function(err, comments) {

		for(i in comments){
			var date = new Date(comments[i].date);
			comments[i].date = moment(date).fromNow();
			comments[i].ISODate = date;
			if(req.user)
				comments[i].superadmin = req.user.superadmin;
			if(comments[i].author == null){
				comments[i].author = {
					_id: '#',
					profile: {
						name: 'Compte supprimé',
						thumbnail: '/images/deleted_account.jpeg'
					}
				};
			}
		}

    res.json(comments);

	});
};

exports.edit = function(req, res){
	req.assert('year', 'Année requise').notEmpty();
	req.assert('month', 'Mois requis').notEmpty();
	req.assert('day', 'Jour requis').notEmpty();
	req.assert('hour', 'Heure requise').notEmpty();
	req.assert('minute', 'Minutes requise').notEmpty();

	var errors = req.validationErrors();

  	if (errors) {
	    req.flash('errors', errors);
	    return res.redirect('/series/'+req.params.tvshow_path_name+'/'+req.params.season_path_name);
 	}

 	var episode = res.locals.episode;

    episode.outDate = new Date(req.body.year, req.body.month-1, req.body.day, req.body.hour, req.body.minute, 0, 0);

  	episode.save(function(err) {
	    if (err) return next(err);
		res.redirect('/series/'+req.params.tvshow_path_name+'/'+req.params.season_path_name+'/'+req.params.episode_path_name);
  	});
};

exports.addSpoilers = function(req, res){

	var episode = res.locals.episode;

	if(req.body.content)
		episode.spoilers = req.body.content;
	if(req.body.urlpreview)
		episode.urlPreview = req.body.urlpreview;

	episode.save(function(err){
		if(err) return(next(err));
		res.redirect('/series/'+req.params.tvshow_path_name+'/'+req.params.season_path_name+'/'+req.params.episode_path_name);
	});

};

exports.addComment = function(req, res) {

	req.assert('content', 'Contenu requis').notEmpty();

	var errors = req.validationErrors();

	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/series/'+req.params.tvshow_path_name+'/'+req.params.season_path_name+'/'+req.params.episode_path_name);
	}

	var tvshow = res.locals.tvshow;
	var season = res.locals.season;
	var episode = res.locals.episode;

	var comment = new Comment({
		author: req.user,
    content: require('./helper').urlify(req.body.content.replace(/</g, "&lt;").replace(/>/g, "&gt;")),
    episode: episode,
    season: season,
    tvshow: tvshow
	});
/*
	var io = require('./socketio').get();
	io.on('connection', function(socket) {
	  socket.emit('newcomment', { 
	  	content: req.body.content, 
	  	author: req.user
	  });
	  socket.on('disconnect', function() {
	    console.log('Socket disconnected');
	  });
	});
*/
	req.user.stats.numberOfComments++;
	req.user.stats.interventions = exports.getIncrementedUserInterventions(req.user, tvshow);

	request.post(
	    'https://www.google.com/recaptcha/api/siteverify?secret='+secrets.recaptcha.key+'&response='+req.body['g-recaptcha-response'],
	    {},
	    function (error, response, body) {
	    	body = JSON.parse(body);
	    	if(body.success){
				comment.save(function(err) {
					if (err) return next(err);
					req.user.save(function(err){
						if (err) return next(err);
						tvshow.numberOfComments++;
						tvshow.save(function(err){
							if (err) return next(err);
							episode.numberOfComments++;
							episode.save(function(err){
								if (err) return next(err);
								res.redirect('/series/'+req.params.tvshow_path_name+'/'+req.params.season_path_name+'/'+req.params.episode_path_name+'?order=-1#comments-container');
							});
						});
					});
				});
			}
			else {
				req.flash('info', { msg: 'Captcha invalide'});
				return res.redirect('/series/'+req.params.tvshow_path_name+'/'+req.params.season_path_name+'/'+req.params.episode_path_name);
			}
		}
	);
};

exports.rate = function(req, res) {

	var episode = res.locals.episode;
	var tvshow = res.locals.tvshow;
	var votedEpisodes = req.user.stats.votedEpisodes;
	var episodeAdded = false;

	for (var i=0; i<votedEpisodes.length; i++) {
		if(votedEpisodes[i].toString() == episode._id.toString()){
			episodeAdded = true
			break;
		}
	}

	if (!episodeAdded){
		votedEpisodes.push(episode);
		episode.votes.push(req.query.note);
	}
	else {
		req.flash('info', { msg: 'Vous avez déjà voté pour cet episode'});
		return res.redirect('/series/'+req.params.tvshow_path_name+'/'+req.params.season_path_name+'/'+req.params.episode_path_name);
	}

	req.user.stats.interventions = exports.getIncrementedUserInterventions(req.user, tvshow);

	episode.save(function(err){
		if (err) return next(err);
		req.user.save(function(err){
			if (err) return next(err);
	    	res.redirect('/series/'+req.params.tvshow_path_name+'/'+req.params.season_path_name+'/'+req.params.episode_path_name);
		});
	});
};

exports.remove = function(req, res) {

	var episode = res.locals.episode;

	async.series([
	  function(next){ Comment.remove({'episode': episode}).exec(next) },
	  function(next){ Episode.remove({'_id': episode._id}).exec(next) }
	], function(err, results){
			if(err)
				req.flash('info', { msg: 'Erreur lors de la suppréssion, réessayez. Erreur : '+err });
			else
				req.flash('info', { msg: 'Episode supprimé' });
			res.redirect('/');
	});

};

exports.removeComment = function(req, res) {
	Comment.remove({'_id': req.params.comment_id}).exec(function(err){
		if(err) 
			req.flash('info', { msg: 'Erreur lors de la suppréssion, réessayez. Erreur : '+err });
		else
			req.flash('info', { msg: 'Commentaire supprimé' });
		res.redirect('/series/'+req.params.tvshow_path_name+'/'+req.params.season_path_name+'/'+req.params.episode_path_name);
	});
};

exports.calculateAverage = function(votes){

	var total = 0;
	for(var i=0; i<votes.length; i++){
		total += votes[i]
	}

	console.log(total);
	return total/votes.length;
};

exports.getIncrementedUserInterventions = function(user, tvshow){
	var interventions = user.stats.interventions;

	var tvshowAdded = false;

	for (var i=0; i<interventions.length; i++) {
		if(interventions[i].id.toString() == tvshow._id.toString()){
			interventions[i].number += 1;
			tvshowAdded = true
			break;
		}
	}

	if (!tvshowAdded){
		interventions.push({id:tvshow, number:1});
	}

	return interventions;

}