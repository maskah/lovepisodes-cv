var async = require('async');
var User = require('../models/User');
var Conversation = require('../models/Conversation');
var Message = require('../models/Message');

var moment = require('moment');
var momentFr = require('moment/locale/fr');

moment.locale('fr', momentFr);

exports.getConversation = function(req, res){

	if(req.query.page) return exports.getNextMessages(req, res);

	Message.find({'conversation': req.params.conversation_id}).sort({'date':-1}).limit(20).populate('author').lean().exec(function(err, messages){
		if(err) return next(err);
		for(i in messages){
			var date = messages[i].date;
			messages[i].date = moment(date).fromNow();
			if(messages[i].author == null){
				messages[i].author = {
					_id: '#',
					profile: {
						name: 'Compte supprimé',
						thumbnail: '/images/deleted_account.jpeg'
					}
				};
			}
		}
		res.render('conversation', {
			title: 'Messagerie',
			messages: messages
		});
	});

	exports.clearNonReadNotifications(req.user, req.params.conversation_id);
};

exports.getNextMessages = function(req, res){

	Message.find({'conversation': req.params.conversation_id}).sort({'date':-1}).skip((req.query.page-1)*20).limit(20).populate('author').lean().exec(function(err, messages){
		if(err) return next(err);
		for(i in messages){
			var date = messages[i].date;
			messages[i].date = moment(date).fromNow();
			if(messages[i].author == null){
				messages[i].author = {
					_id: '#',
					profile: {
						name: 'Compte supprimé',
						thumbnail: '/images/deleted_account.jpeg'
					}
				};
			}
		}
		res.json(messages);
	});
}

exports.getConversations = function(req, res){
	Conversation.find({'users': req.user}).populate('users').populate('lastMessage').limit(30).sort({'lastMessageDate': -1}).lean().exec(function(err, conversations){{}

		for(i in conversations){
			var date = conversations[i].lastMessage.date;
			conversations[i].lastMessage.date = moment(date).fromNow();
			conversations[i].lastMessage.content = conversations[i].lastMessage.content.substring(0, 50)+'...';
			for(j in conversations[i].users){
				if(conversations[i].users[j] == null){
					conversations[i].users[j] = {
						_id: '#',
						profile: {
							name: 'Compte supprimé',
							thumbnail: '/images/deleted_account.jpeg'
						}
					};
				}
			}
			for(j in req.user.nonReadConversations){
				if(req.user.nonReadConversations[j].toString() == conversations[i]._id.toString()){
					conversations[i].nonRead = true;
				}
			}
		}

		exports.clearMessageNotifications(req.user);

		res.render('conversations', {
			title: 'Messagerie',
			conversations: conversations
		});
	});
}

exports.sendMessage = function(req, res, next){

	req.assert('content', 'Contenu requis').notEmpty();

	var errors = req.validationErrors();

	if (errors) {
		req.flash('errors', errors);
		return res.redirect('');
	}

	Conversation.findOne({'_id': req.params.conversation_id}).populate('users').exec(function(err, conversation){
		if (err) return next(err);

		var message = new Message({
	    content: require('./helper').urlify(req.body.content.replace(/</g, "&lt;").replace(/>/g, "&gt;")),
	    conversation: conversation,
	    author: req.user,
	    date: Date.now()
		});

		var conversation = conversation;
		conversation.lastMessage = message;
		conversation.lastMessageDate = message.date;

		message.save(function(err) {
	    	if (err) return next(err);
	    	conversation.save(function(err){
	    		if (err) return next(err);
	    		res.redirect('/conversations/'+req.params.conversation_id);
	    		exports.addMessageNotifications(conversation, req.user);
	    	});
	  	});

	});
};

exports.newConversation = function(req, res){

	if(req.user._id == req.query.recipient) return res.redirect('/conversations');

	User.findOne({'_id':req.query.recipient}).exec(function(err, recipient){
		Conversation.find({"users": req.user}).and({"users": recipient}).exec(function(err, conversations){
  		if(conversations.length!=0) return res.redirect('/conversations/'+conversations[0]._id);
			res.render('newconversation', {
				title: 'Nouvelle conversation',
				recipient: recipient
			});
		});
	});
};

exports.createConversation = function(req, res){

	req.assert('content', 'Contenu requis').notEmpty();

	var errors = req.validationErrors();

	if (errors) {
		req.flash('errors', errors);
		return res.redirect('');
	}

  User.findOne({'_id':req.query.recipient}).exec(function(err, recipient){
  	Conversation.find({"users": req.user}).and({"users": recipient}).exec(function(err, conversations){

  		if(conversations.length!=0) return res.redirect('/conversations/'+conversations[0]._id);

			var conversation = new Conversation({
		    	users: [req.user, recipient]
			});
			var message = new Message({
			    content: require('./helper').urlify(req.body.content.replace(/</g, "&lt;").replace(/>/g, "&gt;")),
			    conversation: conversation,
			    author: req.user,
			    date: Date.now()
			});
			conversation.lastMessage = message;
			conversation.lastMessageDate = message.date;
		  conversation.save(function(err) {
		    if (err) return next(err);
			  message.save(function(err) {
			    if (err) return next(err);
			    res.redirect('/');
			    exports.addMessageNotifications(conversation, req.user);
			  });
		  });
		});
	});
};

exports.addMessageNotifications = function(conversation, user){
	for (var i=0; i<conversation.users.length; i++){
		var _user = conversation.users[i];
		if(_user._id.toString() != user._id.toString()){
			_user.messageNotifications += 1;

			var conversationNonRead = false;

			for(var j=0; j< _user.nonReadConversations.length; j++){
				if (_user.nonReadConversations[j].toString() == conversation._id.toString()){
					conversationNonRead = true;
					break;
				}
			}
			if(!conversationNonRead){
				_user.nonReadConversations.push(conversation);
			}

			_user.save(function(err){
				if (err) return next(err);
			});
		}
	}
};

exports.clearMessageNotifications = function(user){
	user.messageNotifications = 0;
	user.save(function(err){
		if (err) return next(err);
	});
};

exports.clearNonReadNotifications = function(user, conversation_id){
	for(var i=0; i< user.nonReadConversations.length; i++){
		if (user.nonReadConversations[i].toString() == conversation_id){
			user.nonReadConversations.splice(i, 1);
		}
	}
	user.save(function(err){
		if (err) return next(err);
	});
};