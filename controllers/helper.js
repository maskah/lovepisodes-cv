var User = require('../models/User');
var TVShow = require('../models/TVShow');
var Season = require('../models/Season');
var Episode = require('../models/Episode');
var Comment = require('../models/Comment');
var Conversation = require('../models/Conversation');
var crypto = require('crypto');

exports.getTVShowFromPath = function(req, res, next){
	TVShow.findOne({'pathName': req.params.tvshow_path_name}).populate('seasons', null , null, { sort: { 'name': '-1' } }).exec(function(err, tvshow) {

		if (tvshow == null || err) {
			return res.sendStatus(404);
		}

		res.locals.tvshow = tvshow;
		next();

	});
};

exports.getSeasonFromPath = function(req, res, next){

	exports.getTVShowFromPath(req, res, function(){
		Season.findOne({'pathName': req.params.season_path_name, 'tvshow': res.locals.tvshow}).populate('episodes', null, null, { sort: { 'name': '-1' } }).exec(function(err, season) {

			if (season == null || err) {
				return res.sendStatus(404);
			}

			res.locals.season = season;
			next();

		});
	});
};

exports.getEpisodeFromPath = function(req, res, next){

	exports.getSeasonFromPath(req, res, function(){
		Episode.findOne({'pathName': req.params.episode_path_name, 'season': res.locals.season}).exec(function(err, episode) {

			if (episode == null || err) {
				return res.sendStatus(404);
			}

			res.locals.episode = episode;
			next();

		});
	});
};

exports.getConversationFromID = function(req, res, next){
	Conversation.findOne({'_id': req.params.conversation_id}).exec(function(err, conversation) {

		if (conversation == null || err) {
			return res.sendStatus(404);
		}

		if (!exports.isUserHasRightToReadConversation(conversation, req.user))
			return res.sendStatus(403);
		res.locals.conversation = conversation;
		next();

	});
};

exports.isUserHasRightToReadConversation = function(conversation, user){;
	var hasRight = false;
	for(i in conversation.users){
		if(user._id.toString() == conversation.users[i].toString()){
			hasRight = true;
			break;
		}
	}
	return hasRight;
}

exports.getSearchTVShowDatalist = function(req, res, next){
	TVShow.find().select('name').exec(function(err, tvshows){
		if(err) return next();
		res.locals.tvshows = tvshows;
		next();
	});
};

var months = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']

exports.getDate = function(date, year){
	formatedDate = date.getUTCDate() + ' ' + months[date.getMonth()];
	if (year != false) formatedDate += ' ' + date.getUTCFullYear();
	return formatedDate;
};

exports.getHour = function(date){
	return exports.addZero(date.getHours()) + ':' + exports.addZero(date.getMinutes());
};

exports.addZero = function(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
};

exports.gravatar = function(user, size) {
  if (!size) size = 200;
  if (!user.email) return 'https://gravatar.com/avatar/?s=' + size + '&d=retro';
  var md5 = crypto.createHash('md5').update(user.email).digest('hex');
  return 'https://gravatar.com/avatar/' + md5 + '?s=' + size + '&d=retro';
};

exports.urlify = function(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return '<a href="' + url + '">' + url + '</a>';
    })
};